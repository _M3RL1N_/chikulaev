#include <iostream>
#include <cmath> 
using namespace std;

const int MAX_N = 100;
int main()
{
    long int n, x, s, s1, s2, s3, s4;
    long double l;
    cout<< "Введите размер массива n: "; cin>> n;
    while (n > 100)
    {
        cout<< "Размер массива не должен превышать значение больше 100.\n";
        cout<< "Введите размер массива n: "; cin>> n;
    }
    long double m[n];
    for (long int i = 0; i < n; i++)
    {
        cout<<"Введите значение "<< i << "-го " << "элемента массива: "; cin>> l;
        m[i] = l;
    }
    s = 0;
    for (long int k = 0; k < n; k++)
    {
        x = 0;
        for (long int j = 0; j < n; j++)
        {
            if (j != k)
            {
                if (m[k] != m[j]) 
                {
                    x += 1;
                }
            }
        }
        if (x == n - 1)
        {
            s += 1;
        }
    }
    cout<< "Количество различных элементов в массиве: "<< s << endl;
    cout<< "Элементы массива и количество их повторений: \n";
    s1 = 0;
    s2 = 0;
    cout<< "| ";
    for (long int p = 0; p < n; p++)
    {
        if (m[p] == 0)
        {
            s1 += 1;
        }
        else
        {
            s2 += 1;
        }
    }
    if (s1 != 0 && s2 != 0)
    {
        cout<< "0 ("<< s1 <<") | ";
    }
    if (s1 != 0 && s2 == 0)
    {
        cout<< "0 ("<< s1 <<").";
    }
    for (long int z = 0; z < n; z++)
    {
        s3 = 0;
        if (m[z] != 0)
        {
            s4 = m[z];
            for (long int w = 0; w < n; w++)
            {
                if (m[z] == m[w])
                {
                    s3 += 1;
                    if (z != w)
                    {
                        m[w] = 0;
                    }
                }
            }
        }
        m[z] = 0;
        if (s3 != 0)
        {
            cout<< s4 <<" ("<< s3 <<") | ";
        }
    }
    return 0;
}